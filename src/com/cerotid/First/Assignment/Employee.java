package com.First.Assignment;

public class Employee {
	String name = "Hari";
	private int ssn = 123456789;
	double salary = 1235.131;

	public void printEmployeeInfo() {
		printSocialSecurityandName();
		System.out.println("Salary:" + salary);
	}

	public void printSocialSecurityandName() {
		System.out.println("Name:" + name);
		System.out.println("SSN:" + ssn);
	}

	public static void main(String[] args) {
		new Employee().printEmployeeInfo();
		new Employee().printSocialSecurityandName();
	}

}
